import { Messages } from "./interfaces/message.interface";

export const MessagesFr: Messages = {
  isPalindrome: 'Bien dit',
  hello_am: 'Bonjour matin',
  hello_pm: 'Bonjour après-midi',
  hello_evening: 'Bonjour soir',
  hello_night: 'Bonjour nuit',
  bye_am: 'Au revoir matin',
  bye_pm: 'Au revoir après-midi',
  bye_evening: 'Au revoir soir',
  bye_night: 'Au revoir nuit',
};
