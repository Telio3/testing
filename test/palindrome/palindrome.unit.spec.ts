import { MessagesEn } from '../../src/language/const/messages-en.const';
import { MessagesFr } from '../../src/language/const/messages-fr.const';
import { Language, Languages } from '../../src/language/language';
import { DetectionPalindromeBuilder } from './utilities/palindromeDetection.builder';

describe('Palindrome', () => {
  test('the word is reversed', () => {
    // Quand on saisit une chaine
    const palindrome = new DetectionPalindromeBuilder().build();
  
    // Alors celle-ci est renvoyée en miroir
    const result = palindrome.reverse();
  
    expect(result).toEqual('kayak');
  });

  test('detect palindrome word', () => {
    // Quand on saisit une chaine qui est un palindrome
    const palindrome = new DetectionPalindromeBuilder().build();
  
    // Alors celle-ci est renvoyée
    // Et 'Hello' est envoyé avant la chaine
    // Et 'Well said' est envoyé ensuite
    // Et 'Bye' est envoyé en dernier
    const result = palindrome.getResponse();

    expect(result).toEqual(`${MessagesEn.hello_pm} kayak ${MessagesEn.isPalindrome} ${MessagesEn.bye_pm}`);
  });
  
  test('detect non palindrome word', () => {
    // Quand on saisit une chaine qui n'est pas un palindrome
    const palindrome = new DetectionPalindromeBuilder().withWord('abc').build();
  
    // Alors celle-ci est renvoyée en miroir
    // Et 'Hello' est envoyé avant la chaine
    // Et 'Bye' est envoyé en dernier
    const result = palindrome.getResponse();

    expect(result).toEqual(`${MessagesEn.hello_pm} cba ${MessagesEn.bye_pm}`);
  });
  
  test('detect palindrome word with french message', () => {
    // Etant donné un utilsateur parlant français
    // Quand on saisit une chaine qui est un palindrome
    const palindrome = new DetectionPalindromeBuilder().withLanguage(new Language(Languages.FR)).build();
  
    // Alors celle-ci est renvoyée
    // Et 'Bonjour' est envoyé avant la chaine
    // Et 'Bien dit' est envoyé ensuite
    // Et 'Au revoir' est envoyé en dernier
    const result = palindrome.getResponse();
  
    expect(result).toEqual(`${MessagesFr.hello_pm} kayak ${MessagesFr.isPalindrome} ${MessagesFr.bye_pm}`);
  });
  
  test('detect palindrome word with english message', () => {
    // Etant donné un utilsateur parlant anglais
    // Quand on saisit une chaine qui est un palindrome
    const palindrome = new DetectionPalindromeBuilder().withLanguage(new Language(Languages.EN)).build();
  
    // Alors celle-ci est renvoyée
    // Et 'Hello' est envoyé avant la chaine
    // Et 'Well said' est envoyé ensuite
    // Et 'Bye' est envoyé en dernier
    const result = palindrome.getResponse();
  
    expect(result).toEqual(`${MessagesEn.hello_pm} kayak ${MessagesEn.isPalindrome} ${MessagesEn.bye_pm}`);
  });
  
  test('detect palindrome word at 8 am', () => {
    // Etant donné une heure de 8h
    // Quand on saisit une chaine qui est un palindrome
    const palindrome = new DetectionPalindromeBuilder().withDate(new Date('2021-01-01:08:00:00')).build();
  
    // Alors celle-ci est renvoyée
    // Et 'Hello am' est envoyé avant la chaine
    // Et 'Well said' est envoyé ensuite
    // Et 'Bye am' est envoyé en dernier
    const result = palindrome.getResponse();
  
    expect(result).toEqual(`${MessagesEn.hello_am} kayak ${MessagesEn.isPalindrome} ${MessagesEn.bye_am}`);
  });
  
  test('detect palindrome word at 12 pm', () => {
    // Etant donné une heure de 12h
    // Quand on saisit une chaine qui est un palindrome
    const palindrome = new DetectionPalindromeBuilder().withDate(new Date('2021-01-01:12:00:00')).build();
  
    // Alors celle-ci est renvoyée
    // Et 'Hello pm' est envoyé avant la chaine
    // Et 'Well said' est envoyé ensuite
    // Et 'Bye pm' est envoyé en dernier
    const result = palindrome.getResponse();
  
    expect(result).toEqual(`${MessagesEn.hello_pm} kayak ${MessagesEn.isPalindrome} ${MessagesEn.bye_pm}`);
  });
  
  test('detect palindrome word at 18 pm', () => {
    // Etant donné une heure de 18h
    // Quand on saisit une chaine qui est un palindrome
    const palindrome = new DetectionPalindromeBuilder().withDate(new Date('2021-01-01:18:00:00')).build();
  
    // Alors celle-ci est renvoyée
    // Et 'Hello evening' est envoyé avant la chaine
    // Et 'Well said' est envoyé ensuite
    // Et 'Bye evening' est envoyé en dernier
    const result = palindrome.getResponse();
  
    expect(result).toEqual(`${MessagesEn.hello_evening} kayak ${MessagesEn.isPalindrome} ${MessagesEn.bye_evening}`);
  });
  
  test('detect palindrome word at 21 pm', () => {
    // Etant donné une heure de 21h
    // Quand on saisit une chaine qui est un palindrome
    const palindrome = new DetectionPalindromeBuilder().withDate(new Date('2021-01-01:21:00:00')).build();
  
    // Alors celle-ci est renvoyée
    // Et 'Hello night' est envoyé avant la chaine
    // Et 'Well said' est envoyé ensuite
    // Et 'Bye night' est envoyé en dernier
    const result = palindrome.getResponse();
  
    expect(result).toEqual(`${MessagesEn.hello_night} kayak ${MessagesEn.isPalindrome} ${MessagesEn.bye_night}`);
  });

  test('detect if there is no line break', () => {
    // Quand on saisit une chaine qui n'est pas un palindrome
    const palindrome = new DetectionPalindromeBuilder().withWord('abc').build();

    // Alors celle-ci est renvoyée sans retour à la ligne
    const result = palindrome.getResponse();

    expect(result).not.toContain('\r');
  });
});
