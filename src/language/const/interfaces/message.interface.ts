export interface Messages {
  isPalindrome: string;
  hello_am: string;
  hello_pm: string;
  hello_evening: string;
  hello_night: string;
  bye_am: string;
  bye_pm: string;
  bye_evening: string;
  bye_night: string;
}
