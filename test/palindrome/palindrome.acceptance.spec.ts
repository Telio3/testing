import { MessagesEn } from "../../src/language/const/messages-en.const";
import { Language, Languages } from "../../src/language/language";
import { DetectionPalindromeBuilder } from "./utilities/palindromeDetection.builder";
import { LanguageAdapter } from "../../src/language/languageAdapter";
import * as readline from 'readline';
import { PalindromeDetection } from "../../src/palindrome/palindromeDetection";
import { MessagesFr } from "../../src/language/const/messages-fr.const";

describe('Palindrome / acceptance test', () => {
  test('Automatic, palindrome, English, evening', () => {
    // Etant donné que je suis un utilisateur anglais
    // Et qu'il est 20 heures
    // Quand je rentre le mot radar
    const palindrome = new DetectionPalindromeBuilder().withWord('radar').withLanguage(new Language(Languages.EN)).withDate(new Date('2021-01-01:20:00:00')).build();
  
    // Alors celle-ci est renvoyée
    // Et 'Hello evening' est envoyé avant la chaine
    // Et 'Well said' est envoyé ensuite
    // Et 'Bye evening' est envoyé en dernier
    const result = palindrome.getResponse();
  
    expect(result).toEqual(`${MessagesEn.hello_evening} radar ${MessagesEn.isPalindrome} ${MessagesEn.bye_evening}`);
  });
  
  test('Automatic, non-palindrome, French, morning', () => {
    // Etant donné que je suis un utilisateur français
    // Et qu'il est 6 heures
    // Quand je rentre le mot abc
    const palindrome = new DetectionPalindromeBuilder().withWord('abc').withLanguage(new Language(Languages.FR)).withDate(new Date('2021-01-01:06:00:00')).build();
  
    // Alors celle-ci est renvoyée en mirroir
    // Et 'Bonjour matin' est envoyé avant la chaine
    // Et 'Au revoir matin' est envoyé en dernier
    const result = palindrome.getResponse();
  
    expect(result).toEqual(`${MessagesFr.hello_am} cba ${MessagesFr.bye_am}`);
  });

  test('Free input of customer, current system language and time', () => {
    const rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout
    });

    // Etant donné que je suis un utilisateur qui parle une langue
    const language = new Language(LanguageAdapter.getLanguageOfSystem());

    // Et qu'il est une heure
    const date = new Date();

    // Quand on saisit une chaine
    rl.question('Enter a word: ', (word) => {
      const palindrome = new DetectionPalindromeBuilder().withWord(word).withLanguage(language).withDate(date).build();
    
      const isPalindrome = palindrome.isPalindrome();
      const reversedWord = palindrome.reverse();
      const keys = PalindromeDetection.getKeysSchedule(language, date);
    
      // Alors celle-ci est renvoyée

      // Et <salutation> de cette langue à cette période est envoyé avant la chaine
      // CAS {'matin', 'bonjour matin'}
      // CAS {'après-midi', 'bonsoir après-midi'}
      // CAS {'soir', 'bonsoir soir'}
      // CAS {'nuit', 'bonsoir nuit'}

      // Et 'Well said' est envoyé ensuite si la chaine est un palindrome

      // Et <auRevoir> de cette langue à cette période est envoyé en dernier
      // CAS {'matin', 'au revoir matin'}
      // CAS {'après-midi', 'au revoir après-midi'}
      // CAS {'soir', 'au revoir soir'}
      // CAS {'nuit', 'au revoir nuit'}
      
      const result = palindrome.getResponse();

      expect(result).toBe(`${keys.beforeWord} ${isPalindrome ? `${word} ${language.messages.isPalindrome}` : reversedWord} ${keys.afterWord}`);

      rl.close();
    });
  });
});
