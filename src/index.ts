import { Language } from "./language/language";
import { LanguageAdapter } from "./language/languageAdapter";
import { PalindromeDetection } from "./palindrome/palindromeDetection";
import readline from 'readline';

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.question('Enter a word: ', (word) => {
  const ohce = new PalindromeDetection(word, new Language(LanguageAdapter.getLanguageOfSystem()), new Date());

  console.log(ohce.getResponse());

  rl.close();
});
