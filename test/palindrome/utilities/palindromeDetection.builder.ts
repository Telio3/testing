import { Language, Languages } from "../../../src/language/language";
import { PalindromeDetection } from "../../../src/palindrome/palindromeDetection";

export class DetectionPalindromeBuilder {
  private _word: string = 'kayak';
  private _lang: Language = new Language(Languages.EN);
  private _date: Date = new Date('2021-01-01:14:00:00');

  public build(): PalindromeDetection {
    return new PalindromeDetection(this._word, this._lang, this._date);
  }

  public withWord(word: string): DetectionPalindromeBuilder {
    this._word = word;
    return this;
  }

  public withLanguage(lang: Language): DetectionPalindromeBuilder {
    this._lang = lang;
    return this;
  }

  public withDate(date: Date): DetectionPalindromeBuilder {
    this._date = date;
    return this;
  }
}
