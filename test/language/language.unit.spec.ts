import { MessagesEn } from "../../src/language/const/messages-en.const";
import { MessagesFr } from "../../src/language/const/messages-fr.const";
import { Language, Languages } from "../../src/language/language";

describe('Language', () => {
  test('get french messages', () => {
    // Etant donné un utilsateur parlant français
    const lang = new Language(Languages.FR);
  
    // Alors les messages français sont renvoyés
    const result = lang.messages;
  
    expect(result).toEqual(MessagesFr);
  });
  
  test('get english messages', () => {
    // Etant donné un utilsateur parlant anglais
    const lang = new Language(Languages.EN);
  
    // Alors les messages anglais sont renvoyés
    const result = lang.messages;
  
    expect(result).toEqual(MessagesEn);
  });
});
