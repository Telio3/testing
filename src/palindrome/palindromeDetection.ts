import { Language, Languages } from "../language/language";

export class PalindromeDetection {
  private _word!: string;
  private _lang!: Language;
  private _date!: Date;

  constructor(private word: string, private readonly l?: Language, private readonly date?: Date) {
    this._word = word;
    this._lang = l ?? new Language(Languages.EN);
    this._date = date ?? new Date();
  }

  public reverse(): string {
    if (!this._word) return '';

    return this._word.split('').reverse().join('');
  }

  public isPalindrome(): boolean {
    return this._word === this.reverse();
  }

  public static getKeysSchedule(lang: Language, date: Date): { beforeWord: string, afterWord: string } {
    const hour = date.getHours();

    if (hour >= 0 && hour < 12) {
      return { beforeWord: lang.messages.hello_am, afterWord: lang.messages.bye_am };
    }
    else if (hour >= 12 && hour < 18) {
      return { beforeWord: lang.messages.hello_pm, afterWord: lang.messages.bye_pm };
    }
    else if (hour >= 18 && hour < 21) {
      return { beforeWord: lang.messages.hello_evening, afterWord: lang.messages.bye_evening };
    }
    else {
      return { beforeWord: lang.messages.hello_night, afterWord: lang.messages.bye_night };
    }
  }

  public getResponse(): string {
    const keys = PalindromeDetection.getKeysSchedule(this._lang, this._date);

    if (this.isPalindrome()) {
      return [keys.beforeWord, this._word, this._lang.messages.isPalindrome, keys.afterWord].join(' ');
    }
    else {
      return [keys.beforeWord, this.reverse(), keys.afterWord].join(' ');
    }
  }
}
