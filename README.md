## Installer les dépendances

```
pnpm install
```

### Si vous n'avez pas pnpm

```
npm install -g pnpm

pnpm install
```

## Lancer les tests unitaires

```
pnpm run test:unit
```

## Lancer les tests de recette

```
pnpm run test:acceptance
```
