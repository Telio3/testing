import { Messages } from "./interfaces/message.interface";

export const MessagesEn: Messages = {
  isPalindrome: 'Well said',
  hello_am: 'Hello am',
  hello_pm: 'Hello pm',
  hello_evening: 'Hello evening',
  hello_night: 'Hello night',
  bye_am: 'Bye am',
  bye_pm: 'Bye pm',
  bye_evening: 'Bye evening',
  bye_night: 'Bye night',
};
