import { Languages } from "./language";

export class LanguageAdapter {
  public static getLanguageOfSystem(): Languages {
    let language;

    if (process.env.LANG) language = process.env.LANG.split('.')[0];

    switch (language) {
      case Languages.FR:
        return Languages.FR;
      case Languages.EN:
        return Languages.EN;
      default:
        return Languages.EN;
    }
  }
}
