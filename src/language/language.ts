import { Messages } from "./const/interfaces/message.interface";
import { MessagesEn } from "./const/messages-en.const";
import { MessagesFr } from "./const/messages-fr.const";

export enum Languages {
  FR = 'fr_FR',
  EN = 'en_US'
}

export class Language {
  public messages!: Messages;

  constructor(private readonly lang: Languages) {
    this.lang = lang;
    this.messages = this._getResponses();
  }

  private _getResponses(): Messages {
    switch (this.lang) {
      case Languages.FR:
        return MessagesFr;
      case Languages.EN:
        return MessagesEn;
        
      default:
        return MessagesEn;
    }
  }
}
